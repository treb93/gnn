# Graph Neural Network using DGL / PyTorch

## Purpose

Here we adapted an existing GNN code in order to use it in the context of the [H&M Kaggle competition](https://www.kaggle.com/competitions/h-and-m-personalized-fashion-recommendations/discussion/324103).
The underlying library is DGL (Deep Graph Library) with a PyTorch backend.

The original project written by Decathlon's team is available here : https://github.com/je-dbl/GNN-RecSys

Input dataset, parameters and scoring method have been adapted to the needs of the competition.

The model is fully configurable including the number of layers, embedding size, message passing functions etc.
A hyperparameter search with bayesian approach could be performed.

## Refactoring / optimization

Most part of the code has been refactored and optimized in various ways.
**This cleaning process should still be considered as WIP.** Many parts of the code have not been touched yet.

The following updates have been performed :

- Updating the inference system in order to generate predictions for the whole customer file (~ 1 350 000 entries)
- Updating file structure in order to have 1 file = 1 class | method (~60% code covered)
- Creation of a single class `Environment` for [all environment parameters](https://gitlab.com/treb93/gnn/-/blob/master/gnn/environment.py).
- Creation of a single class `Parameters` for [all parameters](https://gitlab.com/treb93/gnn/-/blob/master/gnn/parameters.py) of the model.
- Creation of classes with typed properties for [Dataframes](https://gitlab.com/treb93/gnn/-/blob/master/gnn/src/classes/dataset.py), [Graphs](https://gitlab.com/treb93/gnn/-/blob/master/gnn/src/classes/graphs.py) and [Dataloaders](https://gitlab.com/treb93/gnn/-/blob/master/gnn/src/classes/dataloaders.py) (batches)
- Generate the graph directly from DataFrames instead of using an intermediate dictionary (= avoid crash for big graphs...)
- ban all `for...` loops and dictionnaries in loss / metric calculation.
- Stop using different file paths for search / train / inference mode. Use of a single data source with appropriate internal formatings.
- Improve naming policies for files, methods and variables (avoid `df`, `dict` etc)
- Use of named arguments in functions calls with many parameters.
- Addings some `assert` for controlling Graph generation and some other sensitive process (wip).
- Adding a dataframe saving system for hyperparameter search results.

Still to be done :

- Cut out big methods in respect with the single-reponsibility principle. (i.e in Dataset class)
- Add some assertions / unit tests for sensitive functions (loss, etc)
- Continue to add documented properties in classes, especially for the parameters of the model.
- Extend naming policies to the whole code.
- Clean all unused files.
- Take advantage of the decorator pattern.

### Some examples

<figure><figcaption><b>Example of refactoring. The isolated properties on the left have been replaced by classes with typed properties on the right, which simplifies the code, allows its documentation and enables autocompletion.</b></figcaption><img src="https://gitlab.com/treb93/gnn/-/raw/master/docs/code.png" style="width:100%"></figure>

<figure><figcaption><b>Example of refactoring. Grouping properties into proper classes makes function arguments more readable and less subject to errors.</b></figcaption><img src="https://gitlab.com/treb93/gnn/-/raw/master/docs/code2.png"style="width:100%"></figure>

## Run the code

There are 3 different usages of the code: hyperparametrization, training and inference.
See the notebook [5-1 gnn_prepare_dataset.ipynb](https://gitlab.com/treb93/gnn/-/blob/master/5-1%20gnn_prepare_dataset.ipynb) for data preparation.

### Hyperparametrization

Hyperparametrization is done using the main.py file.
Going through the space of hyperparameters, the loop builds a GNN model, trains it on a sample of training data, and computes its performance metrics.

Metrics are reported in a dataframe for further analysis.

Additionnaly, the metrics are also reported in a result txt file, and the best model's parameters are saved in the models directory.
Plots of the training experiments are saved in the plots directory.
Examples of recommendations are saved in the outputs directory.

```bash
python main.py --from_beginning -v --visualization --check_embedding --remove 0.85 --num_epochs 100 --patience 5 --edge_batch_size 1024 --item_id_type 'ITEM IDENTIFIER' --duplicates 'keep_all'
```

Refer to docstrings of main.py for details on parameters.

### Training

When the hyperparameters are selected, it is possible to train the chosen GNN model on the available data.
This process saves the trained model in the models directory. Plots, training logs, and examples of recommendations are saved.

```bash
python main_train.py --fixed_params_path test/fixed_params_example.pkl --params_path test/params_example.pkl --visualization --check_embedding --remove .85 --edge_batch_size 512
```

Refer to docstrings of main_train.py for details on parameters.

### Inference

With a trained model, it is possible to generate recommendations for all users or specific users.

```bash
python main_inference.py --params_path test/final_params_example.pkl  --user_ids 999 \
--trained_model_path test/final_model_trained_example.pth --k 10 --remove .99
```

Refer to docstrings of main_inference.py for details on parameters.
